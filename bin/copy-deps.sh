#! /bin/sh

mkdir -p dist/node_modules

cp "$(which node)" dist/node
cp bin/run.sh dist/

cp -r node_modules/node-gyp-build dist/node_modules/

cp -r node_modules/sodium-native dist/node_modules/
# prune
rm -rf dist/node_modules/sodium-native/libsodium/test
rm -rf dist/node_modules/sodium-native/libsodium/src

cp -r node_modules/sodium-chloride dist/node_modules/

cp -r node_modules/leveldown dist/node_modules/
