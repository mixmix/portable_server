const caps = require('ssb-caps')

const SSB = require('secret-stack')({ caps })
  .use(require('ssb-db'))

const config = require('./ssb.config.js')()

console.time('ssb startup')
const ssb = SSB(config)

ssb.whoami((err, result) => {
  if (err) throw err
  console.timeEnd('ssb startup')
  console.log('ssb feedId:', result.id)

  ssb.close()
})
