WIP

```bash
$ npm install
$ cd dist
$ ./run.sh
  ssb startup: 17.793ms
  ssb feedId: @wVlIDD9WoiUr0+LC4aotLLMCSYr0O/sPEWx0ONHaXMc=.ed25519
```

install:
- installs node_modules
- postinstall hook:
  - copies native modules into `dist/`
  - copies node binary into `dist/`
    - this assumes you have node installed locally!
    - could pull this from https://nodejs.org/dist/latest-v14.x/
  - uses `noderify` to bundle all modules into `dist/ssb.js`

